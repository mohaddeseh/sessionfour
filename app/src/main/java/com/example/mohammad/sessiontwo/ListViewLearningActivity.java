package com.example.mohammad.sessiontwo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.mohammad.sessiontwo.adapters.StudentListAdapter;

public class ListViewLearningActivity extends AppCompatActivity {

    ListView studentsList;

    PublicMethods pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_learning);
        pm = new PublicMethods(this);
        studentsList = (ListView) findViewById(R.id.studentsList);
        final String names[] = {
                "Mohadese",
                "Yegane",
                "Alireza",
                "Atefeh",
                "Saman",
                "Masoomeh",
                "Amin",
                "Hoopand",
                "Rahim",


        };

        StudentListAdapter adapter = new StudentListAdapter(this ,names);
        studentsList.setAdapter(adapter);
        studentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String names = (String)adapterView.getItemAtPosition(position);

                pm.ShowToast(names);
            }
        });
    }
}
