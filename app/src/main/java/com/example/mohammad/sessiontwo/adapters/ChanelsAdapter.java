package com.example.mohammad.sessiontwo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mohammad.sessiontwo.R;
import com.example.mohammad.sessiontwo.models.ChanelModels;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohammad on 6/12/2017.
 */

public class ChanelsAdapter extends BaseAdapter {
    Context mContext;
    List<ChanelModels> models ;
    Boolean isListView =true ;
    public ChanelsAdapter(Context mContext, List<ChanelModels> models , Boolean isListView) {
        this.mContext = mContext;
        this.models = models;
        this.isListView = isListView ;


    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int i) {
        return models.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = LayoutInflater.from(mContext).inflate(R.layout.chanel_list_item, viewGroup, false);

        if (isListView == false)
            rowView = LayoutInflater.from(mContext).inflate(R.layout.chanel_grid_item, viewGroup, false);


        ImageView ChanelLogo = (ImageView)rowView.findViewById(R.id.chanelLogo);
        TextView ChanelName =(TextView) rowView.findViewById(R.id.chanelName);

        Picasso.with (mContext).load(models.get(i).getChanelLogoURL()).into(ChanelLogo);
        ChanelName.setText(models.get(i).getChanelName());
        return rowView;
    }
}
